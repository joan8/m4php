<!DOCTYPE html>
<html lang="ca">
	<head>
		<meta charset="utf-8">
		<title>PHP</title>
	</head>
	<body>
		<?php
		$varArray=array('Joan','Medina','Gimenez');
		$varArray2=array (
				'id1'=>'DAM',
				'id2'=>'VI',
				'id3'=>'1Mes'
			);
		echo "<p> Mostrem la primera posicio de la Array: </p>";
		echo "<p>".$varArray[0]."</p>";
		echo "<p> Mostrem la segona posicio de la Array: </p>";
		echo "<p>".$varArray[1]."</p>";
		echo "<p> Mostrem la tercera posicio de la Array: </p>";
		echo "<p>".$varArray[2]."</p>";
		echo "<p> Mostrem la primera posicio de la Array associativa: </p>";
		echo "<p>" .$varArray2[id1]. "</p>";
		echo "<p> Mostrem la segona posicio de la Array associativa: </p>";
		echo "<p>" .$varArray2[id2]. "</p>";
		echo "<p> Mostrem la tercera posicio de la Array associativa: </p>";
		echo "<p>" .$varArray2[id3] ."</p>";
		echo "<p> Mostrem la Array: </p>";
		print_r($varArray);
		print_r($varArray2);
		?>
	</body>
</html>
