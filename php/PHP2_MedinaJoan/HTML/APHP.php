<!DOCTYPE html>
 <html lang="ca">
  <head>
    <meta name="author" content="Joan Medina Giménez">
   <meta charset="utf-8">
  <link rel="stylesheet" href="../CSS/APHP.css">
  <title>Activitat 2</title>
  </head>
  <body>
  <header>Activitat 2</header>
<div class="body">
   <?php

	function obData(){
	   $data=date('d/m/Y H:i:s');
	    return $data;
	}

	$arxiu=file($_FILES['fitxer']['tmp_name']);
	$add=-1;
	foreach ($arxiu as $nlinia => $linia){
	   $add=$add+1;
	}
	$escriure=fopen('fitxers/pujada','w');

	foreach ($arxiu as $nfrase => $frase){
	 if($nfrase==$add){
	  fwrite($escriure, $frase."\n"."Text pujat el: ".obData());
	 }else{
	  fwrite($escriure, $frase);
	 }
	}

	fclose($escriure);

	echo "<br>";
	if($_FILES['fitxer']['error']>0){
	   echo "Error en la pujada";
	}else{
	   echo "Tot correcte";
	}
	echo "<br><br>";
	echo "Contingut:";
	$arxiuP=file('fitxers/pujada');
	echo "<br>";
	foreach ($arxiuP as $nfrase2 => $frase2){
	   echo $frase2."<br>";
	}
	echo "<br><br>";
   ?>
</div>
<footer>Joan Medina</footer>
</body>
 </html>
