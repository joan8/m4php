<!DOCTYPE html>
<html lang="ca";>
	<head>
		<meta charset="utf-8">
		<title>PHP</title>
	</head>
	<body>
		<?php
			echo	"<h1>Hello Wolrd!</h1>";
			//Comentari
			/*
			Comentari
			*/
			//variable $
			$var1 = "3";
			$var2 = "5";
			echo "El resultat de multiplicar ".$var1." per ".$var2." es ".$var1*$var2;
			print_r("<br>".$var1."<br>");
			define("PI","3.1416");
			echo PI;
			echo "<p>El valor de pi és: ".PI.".</p>";
			echo "<h2> Arrays en PHP</h2>";
			echo "<h3> Array estàndar</h3>";
			$varArray = array('Element1','Element2','Element3');
			echo "<p>". $varArray[0] ."</p>";
			echo "<p>". $varArray[1] ."</p>";
			echo "<p>". $varArray[2] ."</p>";
			echo "<p>". $varArray ."</p>";
			$varArray = array('Element1','Element2',array('Uno','Dos'));
			echo "<p>". $varArray[0] ."</p>";
			echo "<p>". $varArray[1] ."</p>";
			echo "<p>". $varArray[2][0] ."</p>";
			echo "<p>". $varArray[2][1] ."</p>";
			echo "<h3> Array associatiu</h3>";
			$varArray = array(
					'clau1'=>'Element1',
					'clau2'=>'Element2',
					'clau3'=>'Element3'
					);
			echo "<p>". $varArray[clau1] ."</p>";
			echo "<p>". $varArray[clau2] ."</p>";
			echo "<p>". $varArray[clau3] ."</p>";
			print_r($varArray);
			echo "<p><pre>";
			print_r($varArray);
			echo "</pre></p>"
		?>
	</body>
</html>
